package com.epam.vytrykush.utils;

import java.util.Comparator;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public class NumbersUtil {

    /**
     * create 2 final variables to predicate which type numbers is
     */
    public static final IntPredicate ISODD = n -> n % 2 != 0;
    public static final IntPredicate ISEVEN = n -> n % 2 == 0;

    /**
     * Method determine odd number
     *
     * @param start point our number
     * @param end   point our number
     * @return all Odd numbers
     */
    public static int[] determineOdd(int start, int end) {
        return IntStream.range(start, end + 1)
                .filter(ISODD)
                .toArray();
    }

    /**
     * Method determine even number
     *
     * @param start point our number
     * @param end   point our number
     * @return all Even numbers
     */
    public static int[] determineEven(int start, int end) {
        return IntStream.range(start, end + 1)
                .filter(ISEVEN)
                .boxed()
                .sorted(Comparator.reverseOrder())
                .mapToInt(m -> m)
                .toArray();
    }

    /**
     * Filter method, he take 2 param and return point
     *
     * @param array           fibonacci
     * @param filterPredicate take isOdd or isEven
     * @return one of the choosen result
     */
    public static int[] filter(int[] array, IntPredicate filterPredicate) {
        return IntStream.of(array)
                .filter(filterPredicate)
                .toArray();
    }

    /**
     * Method return max number of user choice
     *
     * @param array numbers
     * @return max odd or even numbers
     */
    public static int getMax(int[] array) {
        return IntStream.of(array)
                .max().getAsInt();
    }

    /**
     * Method calculate and give average of user choice
     *
     * @param array numbers
     * @return average odd or even numbers
     */
    public static double calculateAverage(int[] array) {
        return IntStream.of(array)
                .summaryStatistics().getAverage();
    }

}
