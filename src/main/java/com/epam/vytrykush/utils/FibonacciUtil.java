package com.epam.vytrykush.utils;

public class FibonacciUtil {

    /**
     * Method for fibonacci numbers. Fibonacci Sequence
     * The next number is found by adding up the two numbers before it.
     *
     * @param numbers of fibonacci list
     * @return list of fibonacci numbers
     */
    public static int[] calculate(int numbers) {
        int[] arr = new int[numbers];

        arr[0] = 1;
        arr[1] = 1;
        for (int i = 2; i < arr.length; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
        return arr;
    }
}
