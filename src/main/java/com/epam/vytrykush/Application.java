package com.epam.vytrykush;

import com.epam.vytrykush.user.User;

public class Application {
    public static void main(String[] args) {
        User user = new User();

        System.out.println("----- Interval app -----");
        user.runProgram();

        System.out.println();
        System.out.println("----- Fibonacci app -----");
        user.fibonacciApp();

    }
}
