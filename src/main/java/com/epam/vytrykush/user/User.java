package com.epam.vytrykush.user;

import java.util.Arrays;
import java.util.Scanner;

import static com.epam.vytrykush.utils.FibonacciUtil.calculate;
import static com.epam.vytrykush.utils.NumbersUtil.*;

/**
 * Create a new class where I put some another methods to our client
 */
public class User {

    public User() {
    }

    /**
     * Enter point to our app for interval numbers
     */
    public void runProgram() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter you numbers range (can starts from 1 " +
                "to 99): ");
        int start = scan.nextInt();

        if (start < 1) {    // create range to our first number
            System.out.println("Range can't starts from 0, " +
                    "or negative number, try again: ");
            start = scan.nextInt();
        }

        System.out.println();
        System.out.println("Enter the end of range (end to 100): ");
        int end = scan.nextInt();

        if (end >= 100) {    // create range to our second number
            System.out.println("End range cant be more that 100< try again: ");
            end = scan.nextInt();
        }

        /**
         * Show to user message and list of odd numbers
         */
        System.out.println();
        String odd = Arrays.toString(determineOdd(start, end));
        System.out.println(String.format("Odd numbers: %s", odd));

        /**
         * Show to user message and list of even numbers
         */
        String even = Arrays.toString(determineEven(start, end));
        System.out.println(String.format("Even numbers: %s", even));
    }

    /**
     * Enter point to our app for fibonacci app
     */
    public void fibonacciApp() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter size of generated fibonacci numbers " +
                "(starts from 1): ");
        int size = scan.nextInt();

        /**
         * Create some param for fibonacci:
         * 1: size
         * 2: filter for odd numbers
         * 3: filter for even numbers
         */
        int[] fibonacci = calculate(size);
        int[] odd = filter(fibonacci, ISODD);
        int[] even = filter(fibonacci, ISEVEN);

        /**
         * return max odd and even numbers
         */
        int maxOdd = getMax(odd);
        int maxEven = getMax(even);

        /**
         * Show to user all results
         */
        System.out.println("Fibonacci sequence: " + Arrays.toString(fibonacci));
        System.out.println("Max odd: " + maxOdd);
        System.out.println("Max even: " + maxEven);
        System.out.println("Average odd: " + calculateAverage(odd));
        System.out.println("Average even: " + calculateAverage(even));

    }
}
